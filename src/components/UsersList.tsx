import {Component} from 'react'

import "./UsersList.scss"

interface ICUsers {
    users: IUserForChat[]
}

export class UsersList extends Component<{}, ICUsers> {
    constructor(props: any) {
        super(props);

        this.state = {users: []};
    }

    componentDidMount() {
        window.socket.emit('get-users');

        window.socket.on('users-change', (users: IUserForChat[]) => {
            this.setState({users});
        });
    }

    render() {
        return (
            <div>
                <ul>
                    {this.state.users.map(user => <li key={user.id}>{user.login}</li>)}
                </ul>
            </div>
        )
    }
}