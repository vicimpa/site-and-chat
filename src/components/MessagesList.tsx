import {Component} from 'react'

import "./MessagesList.scss"

export interface IMessage {
    id: string
    autor: string;
    message: string;
    date: Date;
}

export class MessagesList extends Component<{ name: string }, { messages: IMessage[] }> {
    constructor(props) {
        super(props);

        this.state = {messages: []};
    }

    componentDidMount() {
        window.socket.emit('get-messages');

        let setMessages = (messages: IMessage[]) => {
            let box: HTMLDivElement = ((): any => document.querySelector('.messages-box'))();
            let scroll = false;

            if (box.scrollTop < box.scrollHeight - box.offsetHeight)
                scroll = true;

            while (messages.length > 1000)
                messages.shift();

            this.setState({messages});

            if (!scroll)
                box.scrollTop = box.scrollHeight;
        };

        window.socket.on('messages-list', (messages: IMessage[]) => {
            setMessages(messages);
        });

        window.socket.on('new-message', (message: IMessage) => {
            let messages = this.state.messages.concat([]);
            messages.push(message);
            setMessages(messages);
        });
    }

    render() {
        return (
            <div>
                {this.state.messages.map(mess => (
                    <div key={mess.id} className={"message" + (this.props.name === mess.autor ? ' me' : '')}>
                        <div className={"message-container"}>
                            <p className="h4">{mess.autor}</p>
                            <p className="h5">{mess.message}</p>
                        </div>
                    </div>
                ))}
            </div>
        )
    }
}