export function rand(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function range(value: number, max: number = null, min: number = 0) {
    if (max && value > max)
        return max

    if (value < min)
        return min

    return value
}
