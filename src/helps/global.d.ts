declare module "*.scss" {

}

declare function io(url: string): SocketIO.Socket;

declare let socket: SocketIO.Socket;

interface IUserForChat {
    login: string
    id: string
    dateReg: Date
}

interface Window {
    socket: SocketIO.Socket
}