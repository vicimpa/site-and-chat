import "./main.scss"

import * as ReactDOM from 'react-dom'

import {UsersList} from "./components/UsersList";
import {MessagesList} from "./components/MessagesList";

let chat: Element = null;

if ((chat = document.querySelector('.chat'))) {
    window.socket = io(location.host);

    let connec = true;

    window.socket.on('disconnect', () => {
        connec = false;
    });

    window.socket.on('connect', () => {
        if (!connec)
            location.reload();
    });

    let text: HTMLInputElement = ((): any => document.getElementById('message-text'))();
    let btn: HTMLDivElement = ((): any => document.getElementById('message-send'))();

    function sendMessage() {
        if (text.value.length)
            window.socket.emit('send-message', text.value);

        text.value = '';
    }

    btn.onclick = sendMessage;

    function onkeydown(e: KeyboardEvent) {
        if (e.keyCode === 13)
            sendMessage();
    }

    text.onfocus = () => {
        window.addEventListener('keydown', onkeydown);

        text.onblur = () => {
            window.removeEventListener('keydown', onkeydown);
            text.onblur = null;
        }
    };

    //messagesBox
    ReactDOM.render(<UsersList/>, document.getElementById('usersBox'));
    ReactDOM.render(<MessagesList name={chat.getAttribute('data-login')}/>, document.getElementById('messagesBox'));
}
