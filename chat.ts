import {User} from './models'

import {UsersList} from "./chat-app/users";
import {MessagesList} from "./chat-app/messages";

export function main(io: SocketIO.Server) {
    const users = new UsersList(io);
    const messages = new MessagesList(io);

    io.on('connection', async (socket: SocketIO.Socket) => {
        let {user = null, pass = null} = <{ user: string, pass: string }>socket.handshake['session'];
        let findUser = await User.findOne({login: user, password: pass});

        if (!findUser)
            return;

        users.addClient(findUser, socket);
        messages.subscribe(socket);

        socket.on('disconnect', () => {
            users.removeClient(findUser, socket);
        })
    });
}