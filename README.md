#Проект сайтика с чатом

##Зависимости:

1. NodeJS (https://nodejs.org)
2. TypeScript (```npm i -g typescript```)
3. Ruby (https://rubyinstaller.org/)
4. SASS (```gem install sass```)
5. MongoDB (https://www.mongodb.com/)
6. WebPack (```npm i -g webpack```)

##Запуск:

1. ```npm i```
2. ```tsc```  (После компиляции можно закрыть)
3. ```sass public/styles:public/styles --watch```  (После компиляции можно закрыть)
4. ```npm start```
5. ```cd src && npm i && npm start``` (После компиляции можно закрыть)

##Использование:

1. http://localhost:8080

##Сейчас готово:

1. MVC паттерн сервера
2. Отображение статических страниц
3. Регистрация/Авторизация/Logout
4. Базовая версия чата

##Что планируется

1. Версия чата с комнатами и личными сообщениями
2. Добавление Online игр для пользователей
3. Добавление паттерна приложений для пользователей