import {IUser} from '../models'

export interface IUserForChat {
    login: string
    id: string
    dateReg: Date
}

export class ConnectedUser {
    public sockets: SocketIO.Socket[] = [];

    constructor(public user: IUser) {
    }

    public get length(): number {
        return this.sockets.length
    }

    public add(socket: SocketIO.Socket) {
        this.sockets.push(socket)
    }

    public del(socket: SocketIO.Socket) {
        this.sockets = this.sockets.filter(e => e !== socket)
    }
}

export class UsersList {
    public users: ConnectedUser[] = [];

    constructor(public io: SocketIO.Server) {
    }

    public get userNames(): IUserForChat[] {
        let users: IUserForChat[] = [];

        for (let u of this.users)
            users.push({login: u.user.login, id: u.user.id, dateReg: u.user.dateReg})

        return users;
    }

    public get length(): number {
        return this.users.length;
    }

    public getUser(user: IUser): ConnectedUser {
        for (let u of this.users)
            if (u.user.id === user.id)
                return u;

        let newUser = new ConnectedUser(user);
        this.users.push(newUser);
        return newUser;
    }

    public addClient(user: IUser, socket: SocketIO.Socket) {
        let connectedUser: ConnectedUser = this.getUser(user);

        connectedUser.add(socket);

        socket.on('get-users', () => {
            socket.emit('users-change', this.userNames);
        });

        this.io.emit('users-change', this.userNames);

        console.log('Connect client.');
    }

    public removeClient(user: IUser, socket: SocketIO.Socket) {
        let connectedUser: ConnectedUser = this.getUser(user);

        connectedUser.del(socket);

        if (connectedUser.length === 0)
            this.users = this.users.filter(e => e !== connectedUser);

        this.io.emit('users-change', this.userNames);

        console.log('Disconnect client.');
    }
}
