import {Message} from '../models'

export interface IMessageForChat {
    id: string
    autor: string
    message: string
    date: Date
}

export class MessagesList {
    public messages: IMessageForChat[] = [];

    constructor(public io: SocketIO.Server) {
        this.getMessages();
    }

    public async getMessages() {
        let messages = await Message.find({}, null, {limit: 1000});

        for (let mess of messages)
            this.messages.push({id: mess.id, autor: mess.autor, message: mess.message, date: mess.date})

        while (this.messages.length > 1000)
            this.messages.shift();

        return;
    }

    public subscribe(socket: SocketIO.Socket) {
        socket.on('get-messages', () => {
            socket.emit('messages-list', this.messages)
        });

        socket.on('send-message', async (message: string) => {
            if (typeof message !== 'string' || !message.length)
                return;

            let {user} = <{ user: string }>socket.handshake['session'];

            let mess = new Message({autor: user, message: message});
            let messToChat = {id: mess.id, autor: mess.autor, message: mess.message, date: mess.date};

            await mess.save();

            this.messages.push(messToChat);
            this.io.emit('new-message', messToChat);

            console.log(messToChat)
        });
    }
}