import {mongoose} from '../lib'

export let Schema = mongoose.Schema;

export interface IMessage extends mongoose.Document {
    autor: string;
    message: string;
    date: Date;
}

let schema = new Schema({
    autor: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

export let Message = mongoose.model<IMessage>('message', schema, 'messages', true);
