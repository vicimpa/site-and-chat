import {mongoose} from '../lib'

export let Schema = mongoose.Schema;

export interface IUser extends mongoose.Document {
    login: string;
    password: string;
    dateReg: Date;
}

let schema = new Schema({
    login: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    dateReg: {
        type: Date,
        default: Date.now
    }
});

export let User = mongoose.model<IUser>('user', schema, 'users', true);
