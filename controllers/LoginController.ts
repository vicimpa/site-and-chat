import {Controller, md5} from '../lib'
import {User} from '../models'

export class LoginController extends Controller {
    public static init() {
        Controller.all(['*'], this, 'check', 10);

        Controller.get('/login', this, 'login');
        Controller.post('/login', this, 'loginData');

        Controller.get('/register', this, 'register');
        Controller.post('/register', this, 'registerData');

        Controller.get('/logout', this, 'logout');
    }

    public async login() {
        this.data.title = 'Авторизация';
        this.data.header = 'Пожалуйста введите свои данные';

        this.data.page = 'login';

        return this.render('main');
    }

    public async register() {
        this.data.title = 'Регистрация';
        this.data.header = 'Пожалуйста введите свои данные';

        this.data.page = 'register';

        return this.render('main');
    }

    public async loginData() {
        let {login, password} = <{ login: string, password: string }>this.body;

        this.data.preData = {login, password};

        if (!login || !password) {
            this.data.error = 'Вы не ввели логин или пароль!';
            return this.login();
        }

        password = md5(password);

        let findUser = await User.findOne({login, password});

        if (!findUser) {
            this.data.error = 'Не верная комбинация логина и пароля!';
            return this.login();
        }

        this.session.user = findUser.login;
        this.session.pass = findUser.password;

        return this.redirect('/');
    }

    public async registerData() {
        let {login, password, password2} = <{ login: string, password: string, password2: string }>this.body;

        this.data.preData = {login, password, password2};

        if (!login || !password) {
            this.data.error = 'Вы не ввели логин или пароль!';
            return this.register();
        }

        if (login.length < 3) {
            this.data.error = 'Логин не может быть менее 3х символов!';
            return this.register();
        }
        if (password.length < 6) {
            this.data.error = 'Пароль короче 6ти символов запрещен!';
            return this.register();
        }

        if (password !== password2) {
            this.data.error = 'Введенные пароли не совпадают!';
            return this.register();
        }

        if (await User.findOne({login})) {
            this.data.error = 'Пользователь с таким логином уже существует!';
            return this.register();
        }

        password = md5(password);

        let newUser = new User({login, password});

        await newUser.save();

        this.session.user = newUser.login;
        this.session.pass = newUser.password;

        return this.redirect('/');
    }

    public async logout() {
        this.session.user = null;
        this.session.pass = null;

        return this.redirect('/');
    }

    public async check() {
        let {user = null, pass = null} = <{ user: string, pass: string }>this.session;

        if (!this.session.count)
            this.session.count = 0;

        this.session.count = +this.session.count + 1;

        if (!user || !pass)
            return this.data.user = false;

        let findUser = await User.findOne({login: user, password: pass});

        if (!findUser)
            return this.data.user = false;

        this.data.user = findUser;
        return false;
    }
}