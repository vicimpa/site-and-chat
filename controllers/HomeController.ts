import {Controller} from '../lib'

export class HomeController extends Controller {
    public static init() {
        Controller.get('/', this, 'index');
        Controller.get('/chat', this, 'chat');
        Controller.get('*', this, 'notFound', -1);
    }

    public async index() {
        this.data.title = 'Наш супер-пупер чат';
        this.data.header = 'Супер легкий live чат.';

        if (!this.data.user)
            this.data.page = 'notauth';
        else
            this.data.page = 'auth';

        return this.render('main');
    }

    public async chat() {
        if (!this.data.user)
            return this.notAccess();

        this.data.title = 'Чат';
        this.data.header = '';

        this.data.page = 'chat';

        return this.render('main');
    }

    public async notAccess() {
        this.data.title = 'Страница не доступна';
        this.data.header = 'Доступ запрещен!';

        this.data.page = 'access';



        return this.render('main');
    }

    public async notFound() {
        this.data.title = 'Страница не найдена';
        this.data.header = 'Нет такой страницы';

        this.status = 404;

        return this.render('main');
    }
}