import {App, Controller, getSocket, Server, session} from './lib'
import * as ChatApp from './chat'

const server = new Server(App);

ChatApp.main(getSocket(server, session));
Controller.loadControllers('controllers');
Controller.rulesToApp(App);

server.on('request-end', (i) => {
    if (i.status !== 304)
        console.log(`${i.status} [${i.method}] "${i.url}" ${i.time} ms`)
});

server.on('listen', (i) => {
    console.log(`Server listen: "${i.address}:${i.port}"`)
});

server.listen();
