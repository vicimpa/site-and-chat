import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cookieParser from 'cookie-parser'

import {session} from './session'

export const App = express();

App.use(express.static('public'));

App.use(cookieParser());
App.use(session);
App.use(bodyParser.json());
App.use(bodyParser.urlencoded({extended: true}));

App.disable('x-powered-by');

App.set('view engine', 'ejs');
App.set('views', 'templates');