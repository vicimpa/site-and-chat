import * as sIo from 'socket.io'
import {Server} from "./server";
import {RequestHandler} from 'express'

import * as sharedSession from 'express-socket.io-session'

export function getSocket(server: Server, session: RequestHandler): SocketIO.Server {
    const io = sIo(server._s);

    io.use(sharedSession(session, {
        autosave: true
    }));

    return io;
}