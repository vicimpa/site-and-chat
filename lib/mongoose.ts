import * as mongoose from 'mongoose'

function toPromise(mongo: any, promise: any) {
    mongo.Promise = promise;
}

toPromise(mongoose, Promise);

mongoose.connect('mongodb://localhost:27017/site-and-chat', {
    useMongoClient: true
});

export {mongoose}