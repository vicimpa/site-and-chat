import {Express, NextFunction, Request, Response} from 'express'

import * as fs from 'fs'
import * as path from 'path'


type TControlollerClass = new (req: Request, res: Response, next: NextFunction) => Controller
type TRunController = (req: Request, res: Response, next: NextFunction) => void
type TMethod = 'get' | 'post' | 'put' | 'delete' | 'all'

export interface RouteRule {
    url: string | string[],
    ctrl: TControlollerClass,
    method: TMethod,
    action: string,
    p: number
}

export class ControllerStatic {
    public static routeRules: RouteRule[] = [];

    public static run(ctrl: TControlollerClass, action: string): TRunController {
        return (req, res, next) => {
            let c = new ctrl(req, res, next);

            c[action]().then(result => {
                if (typeof result === 'function')
                    try {
                        result = result.apply(c);
                        if (result && result.then) {
                            result.then(e => res.send(e));
                            result.catch(next);
                            return;
                        }
                        return;
                    } catch (e) {
                        return next(e);
                    }

                if (result === false)
                    return next();

                if (result !== undefined)
                    return res.send(result);

            }).catch(next)
        }
    }

    public static get(url: string | string[], ctrl: TControlollerClass, action: string, priority: number = 0) {
        this.routeRules.push({url: url, ctrl: ctrl, method: 'get', action: action, p: priority})
    }

    public static post(url: string | string[], ctrl: TControlollerClass, action: string, priority: number = 0) {
        this.routeRules.push({url: url, ctrl: ctrl, method: 'post', action: action, p: priority})
    }

    public static put(url: string | string[], ctrl: TControlollerClass, action: string, priority: number = 0) {
        this.routeRules.push({url: url, ctrl: ctrl, method: 'put', action: action, p: priority})
    }

    public static delete(url: string | string[], ctrl: TControlollerClass, action: string, priority: number = 0) {
        this.routeRules.push({url: url, ctrl: ctrl, method: 'delete', action: action, p: priority})
    }

    public static all(url: string | string[], ctrl: TControlollerClass, action: string, priority: number = 0) {
        this.routeRules.push({url: url, ctrl: ctrl, method: 'all', action: action, p: priority})
    }

    public static rulesToApp(app: Express) {
        let rules = this.routeRules.concat([]).sort((a, b) => {
            if (a.p < b.p)
                return 1;
            if (a.p > b.p)
                return -1;
            return 0;
        });

        for (let rule of rules)
            app[rule.method](rule.url, this.run(rule.ctrl, rule.action))
    }

    public static loadControllers(folder: string = '.') {
        folder = path.join(process.cwd(), folder);

        let files = fs.readdirSync(folder);

        for (let file of files) {
            if (/\.js$/.test(file)) {
                let obj: any = require(path.join(folder, file));

                for (let i in obj) {
                    let prop = obj[i];

                    if (prop.init)
                        prop.init();
                }
            }
        }
    }
}

export class Controller extends ControllerStatic {
    constructor(private req: Request, private res: Response, private next: NextFunction) {
        super()
    }

    public get data(): any {
        return this.req['data'] || (this.req['data'] = {})
    }

    public get body(): any {
        return this.req.body
    }

    public get query(): any {
        return this.req.query
    }

    public get params(): any {
        return this.req.params
    }

    public get session(): any {
        return this.req.session
    }

    public get status(): number {
        return this.res.statusCode
    }

    public set status(v: number) {
        this.res.statusCode = v
    }

    public send(data: any) {
        this.res.send(data)
    }

    public write(data: string | Buffer) {
        this.res.write(data)
    }

    public redirect(url: string) {
        this.res.redirect(url);
    }

    public render(view: string) {
        return () => {
            this.res.render(view, {data: this.data})
        }
    }
}