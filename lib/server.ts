import * as http from 'http'
import {IncomingMessage, Server as HttpServer, ServerResponse} from 'http'
import * as EventEmitter from 'events'

type THandler = (req: IncomingMessage, res: ServerResponse) => void | Promise<void>

export interface IInfoRequest {
    url: string,
    method: string,
    status: number,
    time: number
}

export interface IInfoListen {
    address: string,
    port: number,
    family: string
}

export interface Server {
    on(type: 'request', listener: THandler): void

    on(type: 'request-end', listener: (info: IInfoRequest) => void): void

    on(type: 'listen', listener: (info: IInfoListen) => void): void
}

export class Server {
    public _s: HttpServer = null;
    public _e: EventEmitter = null;

    constructor(handler: THandler = null) {
        this._s = http.createServer(handler);
        this._e = new EventEmitter();

        this._s.on('request', (req: IncomingMessage, res: ServerResponse) => {
            let end = res.end, time = Date.now();

            this._e.emit('request', req, res);

            res.end = (...args: any[]) => {
                let info: IInfoRequest = {
                    url: req.url,
                    method: req.method,
                    status: res.statusCode,
                    time: Date.now() - time
                };

                end.apply(res, args);

                this._e.emit('request-end', info);
            }
        });

        this._s.on('listening', () => {
            let info: IInfoListen = this._s.address();

            this._e.emit('listen', info);
        })
    }

    public on(...args: any[]) {
        this._e.on.bind(this._e)(...args)
    }

    public off(...args: any[]) {
        this._e.removeListener.bind(this._e)(...args)
    }

    public once(...args: any[]) {
        this._e.once.bind(this._e)(...args)
    }

    public listen(port: number = 8080, address: string = '0.0.0.0') {
        this._s.listen(port, address);
    }
}