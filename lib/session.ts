import * as Session from 'express-session'
import * as mongo from 'connect-mongo'
import {mongoose} from './mongoose'

export const MongoStore = mongo(Session);

export const session = Session({
    secret: 'secretChatString',
    saveUninitialized: true,
    resave: false,
    store: new MongoStore({mongooseConnection: mongoose.connection})
});